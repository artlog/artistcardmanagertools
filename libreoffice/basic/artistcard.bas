REM  *****  BASIC  *****


REM Compute pairs for artist card exchange ( https://en.wikipedia.org/wiki/Artist_trading_cards )
REM See project https://framagit.org/artlog/artistcardmanagertools
REM License GPLv3 see full licensing term here https://www.gnu.org/licenses/gpl-3.0.html
REM Copyleft by artlog

Option Explicit

Function AddExchange(ByVal userA as Integer,ByVal userB as Integer, ByVal pair as Variant, ByVal cards as Variant)
	
	If pair(userA,userB) = 0 Then
		pair(userA,userB) = 1
		cards(userA) = cards(userA) - 1
		AddExchange = 1
	Else
		AddExchange = 0
	EndIf


End Function

Function AddUserPair(ByVal userA as Integer,ByVal userB as Integer, ByVal pair as Variant, ByVal cards as Variant) as Integer
	
	Dim t as String
	REM t = " " & userA & " <-> " & userB
	REM MsgBox t
	
	If pair(userA,userB) = 0 Then
		AddUserPair = AddExchange(userA,userB,pair,cards) + AddExchange(userB,userA,pair,cards)
	End If
	
End Function

Function GetRemainingCards(ByVal lastuser as Integer,  ByVal cards as Variant)

Dim i,remainingcards as Integer

For i = 0 To lastuser
	remainingcards = remainingcards + cards(i)
Next i

GetRemainingCards = remainingcards

End Function

REM This is independant from Calc functions
REM nuser is an Array taht is non 0 if user is not fully computed, allows to select a subset of users
REM cards is number of cards remaining to distribute for a given user
REM pair is actual result of this
REM quadratic complexity cpu and mem
Function CreateAllPairs(ByVal nuser as Variant, ByVal cards as Variant, ByVal pair as Variant)

Dim lastuser as Integer

lastuser = UBound(nuser) - LBound(nuser) - 1
MsgBox "lastuser=" & lastuser

# first user is first element of list
Dim firstuser, i, ppair, luser as Integer
firstuser = 0
ppair = 0

Dim remainingcards, lastremaining as Integer

remainingcards = GetRemainingCards(lastuser,cards)
lastremaining = 0

While remainingcards > 1 and lastremaining <> remainingcards

firstuser = 0

REM reset list of users having remaining cards
For i = 0 To lastuser
	REM nuser(i) = ( cards(i) > 0 )
	If  cards(i) > 0 Then
		nuser(i) = 1
	Else
		nuser(i) = 0
	End If	
Next i

lastremaining = remainingcards

REM distribute next card, one at a time
For luser = 0 to lastuser
	If nuser(luser) > 0 Then			
		
		REM actualy remove this user
		firstuser = luser + 1
		nuser(luser) = 0
		
		If cards(luser) > 0 Then
			REM need to find a pair
			For ppair = firstuser to lastuser
				If nuser(ppair) > 0 Then
					If cards(ppair) > 0 Then			
						If AddUserPair(luser,ppair,pair,cards) > 0 Then
							REM remove user from nuser list
							If ppair = firstuser Then
								firstuser = firstuser + 1
							EndIf
							nuser(ppair) = 0
							Exit For
						EndIf
					EndIf
				Else
					REM fixup first element of list if head was removed
					If ppair = firstuser Then
						firstuser = firstuser + 1
					EndIf
				EndIf
			Next ppair
		EndIf	
		
	EndIf
Next luser

remainingcards = GetRemainingCards(lastuser,cards)

REM MsgBox "" & lastremaining & " " & remainingcards

Wend

CreateAllPairs=remainingcards

End Function


Sub DistributeCards

Dim maxuser, lastuser as Integer
maxuser = 500
lastuser = maxuser

Dim oSheet As Variant 
oSheet = ThisComponent.CurrentController.getActiveSheet 
Dim oRange,oCell As Object
Dim strRange As String

Dim iline,firstline,firstcolumn,i as Integer
REM first line is 1 ( not 0 ) and first line is for column title
firstline=2
# G
firstcolumn=6

strRange = "G2:AZ" & maxuser
oRange = oSheet.getCellRangebyName( strRange )

Dim doit as Integer
doit = MsgBox("This " & strRange &  " range will be removed and recomputed ",MB_OKCANCEL , "Distribute Cards")

If doit <> 1 Then
MsgBox("Action canceled")
Exit Sub
End If

Dim rangeAddress as Object
rangeAddress=oRange.RangeAddress

REM For some reason this does not empty cells :
REM oSheet.removeRange(rangeAddress, com.sun.star.sheet.CellDeleteMode.NONE)
REM needed to use LEFt ...
oSheet.removeRange(rangeAddress, com.sun.star.sheet.CellDeleteMode.LEFT)

strRange = "A1:AZ" & (lastuser + firstline )
oRange = oSheet.getCellRangebyName( strRange )


For i = 0 To lastuser
REM	cards(i) = Int((lastuser * Rnd) )
	oCell = oRange.getCellByPosition( 2, i + 1)	
	REM Autodetect last user if cell is empty
	If ( oCell.TYPE = com.sun.star.table.CellContentType.EMPTY ) Then
		lastuser = i - 1
		Exit For
	End If
Next i

Dim nuser(lastuser+1)
Dim pair(lastuser+1,lastuser+1) as Integer

REM number of cards this user want to send
Dim cards(lastuser+1) as Integer

For i = 0 To lastuser
REM	cards(i) = Int((lastuser * Rnd) )
	oCell = oRange.getCellByPosition( 2, i + 1)	
	REM Autodetect last user if cell is empty
	If ( oCell.TYPE = com.sun.star.table.CellContentType.EMPTY ) Then
		lastuser = i - 1
		Exit For
	End If
REM	MsgBox "nombre de cartes " & oCell.Value
	cards(i) = Int(oCell.Value)
	nuser(i) = 1
Next i

Dim remainingcards as Integer
remainingcards = CreateAllPairs(nuser,cards,pair)

MsgBox "remaining undistributed cards " & remainingcards
	
Dim luser,ppair as Integer

REM collect all results ...
For luser = 0 to lastuser

	Dim pliststr as String
	pliststr = "" 
			
	For ppair = 0 to lastuser
		If pair(luser,ppair) > 0 or pair(ppair,luser) >  0 Then
			pliststr = pliststr & " " & ppair
		End If
	Next ppair
	
	pliststr = trim(pliststr)
	
	iline = luser + firstline - 1

	oCell = oRange.getCellByPosition( firstcolumn, iline )
	oCell.Value = luser
	oCell = oRange.getCellByPosition( firstcolumn + 1, iline )
	oCell.Formula = "=E" & (luser + firstline)
	oCell = oRange.getCellByPosition( firstcolumn + 2, iline )
	oCell.String = pliststr	
	If ( cards(luser) > 0 )	Then
		oCell = oRange.getCellByPosition( firstcolumn + 3, iline )
		oCell.Value = cards(luser)
	End If
	Dim cpair as Integer
	oCell = oRange.getCellByPosition( firstcolumn + 4 , iline )
	Dim str as String
	REM JOINDRE.TEXTE in french but this is in default macro
	str = "=TEXTJOIN("","";1;K" & ( luser + firstline ) & ":AZ" & (luser + firstline) & ")"
	oCell.Formula = str
	cpair=0
	For ppair = 0 to lastuser
		If pair(luser,ppair) > 0 Then
			oCell = oRange.getCellByPosition( firstcolumn + 4 + cpair , iline )
			oCell.Formula = "=F" & ( ppair + firstline )
			cpair = cpair + 1
		End If
	Next ppair
	
	REM MsgBox strRange & " utilisateur " & luser & ":" & pliststr
			
Next luser		


End Sub
